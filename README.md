# README #

*This is a rough and ready reaction time app that saves data locally on device as well as sending data to a server
* to see the data from the server go to http://scm.ulster.ac.uk/~b00565433/ and use the login 'edward' and 'password' (decided to implement a very simple login system on the site as it might be needed later down the line. registration should also work)
*data is displayed on a very simple chart on the site which might break in future when there is too much data on the server


### Summary ###

*version 0.1


### Owner ###

*Edward Price
*price-e@email.ulster.ac.uk