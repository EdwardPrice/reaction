//
//  SecondViewController.swift
//  Reactions
//
//  Created by Edward Price on 20/11/2014.
//  Copyright (c) 2014 Edward Price. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mean: UILabel!
    @IBOutlet weak var Median: UILabel!
    
    @IBOutlet weak var morningReactions: UILabel!
    @IBOutlet weak var eveningReactions: UILabel!
    @IBOutlet weak var nightReactions: UILabel!
    
    let cellIdentifier = "Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "\"The List\""
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        //take all of the time data from the data in the database and put it into a array for calculations
        var arrayForCalculations: Array<Double> = []
        
        for index in reactions.results{
            var item = index.valueForKey("time") as Double
            arrayForCalculations.append(item)
        }
        
        var count = reactions.results.count
        
        //if there is less than one piece of data returned then dont do the mean and median calculations
        if count < 1 {
        }
        else{
            var theAverage: Double = calculateAverage(arrayForCalculations)
            mean.text = String(format:"%.2f", theAverage) + " seconds"
            
            var theMedian: Double = calculateMedian(arrayForCalculations)
            Median.text = String(format:"%.2f", theMedian) + " seconds"
        }
        
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //MOVE THIS INTO A GOD DAM SEPERATE FUNCTION!! FLIP SAKE
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        
        if reactions.results.count != 0 {
            var timesDictionary = Dictionary<Double, Float>()
            
            for index in reactions.results{
                var reaction = index.valueForKey("time") as Double
                
                let date: NSDate = index.valueForKey("timeOfDay") as NSDate
                let calendar = NSCalendar.currentCalendar()
                let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: date)
                let hour = components.hour
                let minutes = components.minute
                
                println(hour, minutes)
                
                var myStringOne = String(hour)
                var myStringTwo = String(minutes)
                
                var initial: String  = myStringOne + "." + myStringTwo
                
                var finalTimeValue = (initial as NSString).floatValue
                
                //var double = Double(initial.toInt()!)
                
                println("three")
                timesDictionary[reaction] = finalTimeValue
                println("four")
            }
            
            var morning: Array<Double> = []
            var evening: Array<Double> = []
            var night: Array<Double> = []
            
            for (reaction, timeOfDay) in timesDictionary {
                
                if timeOfDay < 12 {
                    println("morning")
                    morning.append(reaction)
                }
                    
                else if timeOfDay > 12 && timeOfDay < 18 {
                    println("evening")
                    evening.append(reaction)
                }
                    
                else if timeOfDay > 18 && timeOfDay < 24 {
                    println("night")
                    night.append(reaction)
                }
            }
            
            var morningAverage: Double = calculateAverage(morning)
            morningReactions.text = String(format:"%.2f", morningAverage) + " seconds"
            
            var eveningAverage: Double = calculateAverage(evening)
            eveningReactions.text = String(format:"%.2f", eveningAverage) + " seconds"
            
            var nightAverage: Double = calculateAverage(night)
            nightReactions.text = String(format:"%.2f", nightAverage) + " seconds"
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //MOVE THIS INTO A GOD DAM SEPERATE FUNCTION!! FLIP SAKE
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
    }
    
    //////////////////////////////////////
    //calculate mean and average functions
    //////////////////////////////////////
    
    func calculateAverage(Array: [Double]) -> Double {
        var total: Double = 0
        for value in Array[0..<Array.count] {
            total += value
        }
        return total / Double(Array.count)
    }
    
    func calculateMedian(Array: [Double]) -> Double {
        var result = sorted(Array)
        //println(result)
        var median: Double
        
        if Array.count % 2 == 0 {
            median = (result[(result.count/2)] + result[(result.count/2)-1]) / 2
        }
        else if Array.count == 1{
            median = result[0]
        }
        else{
            median = result[((result.count) / 2)]
        }
        
        return median
    }
    //////////////////////////////////////////
    //calculate mean and average functions End
    /////////////////////////////////////////
    
    
    
    
    //////////////////////////////////////////
    //set up how many table rows have data in them based on the count of results returned, 
    //if none is returned then count is set to one to display the msg no data
    /////////////////////////////////////////
    
    // MARK: UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = reactions.results.count
        
        if count < 1 {
            return 1
        }

        else{
            return count
        }
    }
    //////////////////////////////////////////
    /////////////////////////////////////////
    
    
    
    
    //////////////////////////////////////////
    //set up what is in each table row based on the data in the database
    /////////////////////////////////////////
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as UITableViewCell
        
        var count = reactions.results.count
        if count < 1 {
            cell.textLabel?.text = "No data"
        }
            
        else{
            let time = reactions.results[indexPath.row]
            
            var timeResult: Double = time.valueForKey("time") as Double
            
            var b:String = String(format:"%.2f", timeResult) + " seconds"
            
            cell.textLabel?.text = b
        }
        
        return cell
    }
    //////////////////////////////////////////
    /////////////////////////////////////////
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
