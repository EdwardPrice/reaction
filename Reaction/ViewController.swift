//
//  ViewController.swift
//  Reactions
//
//  Created by Edward Price on 19/11/2014.
//  Copyright (c) 2014 Edward Price. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    //the background (changes colour) result label which is in the middle of the screen 
    //and the instructions which is below result
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var instruction: UILabel!
    
    //global reference of the start time of the reaction
    var startTime = NSTimeInterval()
    
    //two timers one for the 1sec delay before the random delay happens
    var timer = NSTimer()
    var delay = NSTimer()
    //timer for if the reaction time is longer than 4 secs
    var reactionSlow = NSTimer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    ////////////////////////////////////////////
    // Set background and get current saved data
    ///////////////////////////////////////////
    
    override func viewWillAppear(animated: Bool) {
        background.tag = 1
        
        super.viewWillAppear(animated)
        
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Times")
        
        //3
        var error: NSError?
        
        let fetchedResults = managedContext.executeFetchRequest(fetchRequest, error: &error) as [NSManagedObject]?
        
        //println(fetchedResults)
        
        if let theResults = fetchedResults {
            reactions.results = theResults
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    ////////////////////////////////////////////
    // Set background and get current saved data
    ///////////////////////////////////////////
    
    
    
    
    
    /////////////////////////////
    // Touch event on the screen
    /////////////////////////////

    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        super.touchesBegan(touches, withEvent: event)
        
        for touch: AnyObject in touches {
            //run the tapped function
            tapped()
        }
    }
    /////////////////////////////
    // Touch event on the screen
    /////////////////////////////
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////
    // check what state the screen is in and do the appropiate action for the screen
    ///////////////////////////////////////////////////////////////////////////////

    func tapped() {
        
        if background.tag == 1{
            
            reactionSlow.invalidate()
            background.tag = 2
            background.backgroundColor = UIColor.redColor()
            instruction.text = "Wait..."
            result.text = ""
            delay = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("randomShowPrompt"), userInfo: nil, repeats: false)
        }
            
        else if background.tag == 2{
            
            reactionSlow.invalidate()
            background.tag = 1
            instruction.text = "Tap to start again"
            result.text = "Too Soon!!"
            timer.invalidate()
            delay.invalidate()
        }
            
        else if background.tag == 3{
            
            var currentTime = NSDate.timeIntervalSinceReferenceDate()
            reactionSlow.invalidate()
            background.backgroundColor = UIColor(red: 93/225, green: 188/225, blue: 210/225, alpha: 1.0)
            background.tag = 1
            instruction.text = "Well done. Tap to start again"
            
            //Find the difference between current time and start time.
            var elapsedTime: NSTimeInterval = currentTime - startTime
            
            
            var a:String = String(format:"%.2f", elapsedTime)
            result.text = "Reaction was: " + a + " seconds"
            
            var date = NSDate()

            saveName(elapsedTime, date: date)
            saveToDB(elapsedTime, date: date)
        }
        
    }
    /////////////////////////////
    // End of screen check state
    /////////////////////////////
    
    
    
    
    ///////////////////////
    //Start of random Timer
    ///////////////////////
    
    func randomShowPrompt(){
        // Seconds before performing next action. Choose a default value
        var timeUntilNextAction = CDouble(arc4random_uniform(5))
        
        timer = NSTimer.scheduledTimerWithTimeInterval(timeUntilNextAction, target: self, selector: Selector("showPrompt"), userInfo: nil, repeats: false)
        
    }
    
    func showPrompt() {
        startTime = NSDate.timeIntervalSinceReferenceDate()
        background.backgroundColor = UIColor.greenColor()
        instruction.text = "GO!"
        background.tag = 3
        reactionSlow = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: Selector("tooSlow"), userInfo: nil, repeats: false)
    
    }
    
    func tooSlow(){
        background.tag = 1
        background.backgroundColor = UIColor.redColor()
        result.text = "Tap to start again"
        instruction.text = "No reaction detected"
        timer.invalidate()
        delay.invalidate()
    }
    /////////////////////
    //End of random timer
    /////////////////////

    
    
    ///////////////////////////
    //Save the data to core data
    ///////////////////////////
    
    func saveName(time: Double, date: NSDate) {
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let entity =  NSEntityDescription.entityForName("Times", inManagedObjectContext: managedContext)
        
        let times = NSManagedObject(entity: entity!, insertIntoManagedObjectContext:managedContext)
        
        //3
        times.setValue(time, forKey: "time")
        times.setValue(date, forKey: "timeOfDay")
        
        //4
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }  
        //5
        reactions.results.append(times)
        
    }
    ///////////////////////////
    //Save the data to core data
    ///////////////////////////
    
    
    func saveToDB(time: Double, date: NSDate) {
        
        
        var formattedTime = String(format:"%.2f", time)
        
        var formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        let dateTimePrefix: String = formatter.stringFromDate(date)
        
        var uniqueID = UIDevice.currentDevice().identifierForVendor.UUIDString
        
        var bodyData = "userId=\(uniqueID)&reactionTime=\(formattedTime)&date=\(dateTimePrefix)" //To get them in php: $_POST['name']
        
        let URL: NSURL = NSURL(string: "https://scm.ulster.ac.uk/~b00565433/service.php")!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:URL)
        request.HTTPMethod = "POST"
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue())
            {
                (response, data, error) in
                println(response)
            }
    }
    
    
    ///////////////////////////
    //settings button functionality
    ///////////////////////////
    
    @IBAction func settingsScreen(sender: AnyObject) {
        timer.invalidate()
        delay.invalidate()
        reactionSlow.invalidate()
        instruction.text = "Tap to start"
        background.tag = 1
        background.backgroundColor = UIColor(red: 93/225, green: 188/225, blue: 210/225, alpha: 1.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//////////////////////////////////
//Globally accessable times objects
//////////////////////////////////

class reactionResults {
    var results = [NSManagedObject]()
}
var reactions = reactionResults()

//////////////////////////////////
//Globally accessable times objects
//////////////////////////////////

